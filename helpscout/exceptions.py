"""
Helpscout exceptions package.
"""
from requests import Response


class HSError(Exception):
    """
    Base HSError exception class. All other exception types are derived from
    this one.
    """

    msg = None

    def __init__(self, response: Response):
        pass
