"""
Helpscout API wrapper module.
"""
from typing import Dict, Callable

import requests
from requests_oauthlib import OAuth2Session

from helpscout.exceptions import HSError


class API:
    """
    Base class for API interactions.
    """

    _href = 'https://api.helpscout.net'

    _app_id = ""  # type: str
    _app_secret = ""  # type: str
    _client = None  # type: OAuth2Session

    def __init__(self, app_id: str, app_secret: str, token: Dict = None,
                 token_saver: Callable[[Dict], None] = None):
        self._app_id = app_id
        self._app_secret = app_secret

        self._client = OAuth2Session(
            client_id=self._app_id,
            token=token,
            auto_refresh_url='{}/oauth2/token'.format(self._href),
            token_updater=token_saver,
        )

    def get_authorization_url(self) -> str:
        """
        Returns authorization url for app authorization.
        """
        authorization_url, _ = self._client.authorization_url(
            'https://secure.helpscout.net/authentication'
            '/authorizeClientApplication',
        )

        return authorization_url

    def get_token(self, code: str):
        """
        Returns token in exchange to code provided by authorization step.
        """
        token = self._client.fetch_token(
            token_url='{}/oauth2/token'.format(self._href),
            client_id=self._app_id,
            client_secret=self._app_secret,
            code=code,
        )

        return token

    def get(self, url: str):
        """
        Gets data from API.
        """
        response = self.request('get', url)

        if response.status_code == 200:
            return response.json()

    def post(self, url: str, data: dict):
        """
        Posts data to API.
        """
        response = self.request('post', url, data=data)

        if response.status_code == 201:
            return True

    def request(
            self, method: str, url: str, *args, **kwargs
    ) -> requests.Response:
        """
        Performs an authenticated request.
        """
        response = self._client.request(
            method=method,
            url="{}{}".format(self._href, url),
            *args,
            **kwargs,
            client_id=self._app_id,
            client_secret=self._app_secret
        )

        # https://developer.helpscout.com/mailbox-api/overview/status_codes/
        if response.status_code >= 400:
            raise HSError(response)

        return response
